package com.buckstter.Activities.MiCuenta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.buckstter.Activities.Productos.DescripcionActivity;
import com.buckstter.Adapter.AdapterFotos;
import com.buckstter.Objetos.Fotos;
import com.buckstter.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

public class VenderActivity extends AppCompatActivity implements AdapterFotos.EventListener {

    List<Fotos> fotos;
    AdapterFotos adapterFotos;
    RecyclerView fotosList;
    FloatingActionButton agregar;
    CardView titulo, precio, cantidad, caracteristicas, descripcion, pago, publicacion;
    TextView tituloText, precioText, cantidadText, caracteristicasText, descripcionText, pagoText, publicacionText;
    Button publicar;
    private static final int RESULT_LOAD_IMG = 1010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vender);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fotosList = findViewById(R.id.fotosList);
        agregar = findViewById(R.id.agregar);
        titulo = findViewById(R.id.titulo);
        tituloText = findViewById(R.id.tituloText);
        precio = findViewById(R.id.precio);
        precioText = findViewById(R.id.precioText);
        cantidad = findViewById(R.id.cantidad);
        cantidadText = findViewById(R.id.cantidadText);
        caracteristicas = findViewById(R.id.caracteristicas);
        caracteristicasText = findViewById(R.id.caracteristicasText);
        descripcion = findViewById(R.id.descripcion);
        descripcionText = findViewById(R.id.descripcionText);
        pago = findViewById(R.id.pago);
        pagoText = findViewById(R.id.pagoText);
        publicacion = findViewById(R.id.publicacion);
        publicacionText = findViewById(R.id.publicacionText);
        publicar = findViewById(R.id.publicar);
        fotosList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        fotos = new ArrayList<>();
        adapterFotos = new AdapterFotos(this, fotos, this);
        fotosList.setAdapter(adapterFotos);
        fotosList.setFocusable(false);

        titulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_input, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final TextInputLayout dialogLayout = v.findViewById(R.id.textoLayout);
                final TextInputEditText dialogTexto = v.findViewById(R.id.texto);

                dialogTitulo.setText("Título de la Publicación");
                dialogLayout.setHint("Título");

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                Toast.makeText(VenderActivity.this, dialogTexto.getText().toString(), Toast.LENGTH_SHORT).show();
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int check = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if (check != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    VenderActivity.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1024);
                } else {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                }
            }
        });

        precio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_input, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final TextInputLayout dialogLayout = v.findViewById(R.id.textoLayout);
                final TextInputEditText dialogTexto = v.findViewById(R.id.texto);

                dialogTitulo.setText("Precio del Producto");
                dialogLayout.setHint("Precio");

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                Toast.makeText(VenderActivity.this, dialogTexto.getText().toString(), Toast.LENGTH_SHORT).show();
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        cantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_input, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final TextInputLayout dialogLayout = v.findViewById(R.id.textoLayout);
                final TextInputEditText dialogTexto = v.findViewById(R.id.texto);

                dialogTitulo.setText("Unidades Disponibles");
                dialogLayout.setHint("Cantidad");

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                Toast.makeText(VenderActivity.this, dialogTexto.getText().toString(), Toast.LENGTH_SHORT).show();
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        caracteristicas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_caracteristicas, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextInputEditText marca = v.findViewById(R.id.marca);
                final TextInputEditText modelo = v.findViewById(R.id.modelo);
                final Spinner calibre = v.findViewById(R.id.calibre);
                final Spinner categoria = v.findViewById(R.id.categoria);

                // you need to have a list of data that you want the spinner to display
                List<String> spinnerCalibre =  new ArrayList<String>();
                spinnerCalibre.add("9x19mm");
                spinnerCalibre.add(".22 L.R.");

                List<String> spinnerCategoria =  new ArrayList<String>();
                spinnerCategoria.add("Pistolas");
                spinnerCategoria.add("Revólveres");
                spinnerCategoria.add("Carabinas");
                spinnerCategoria.add("Escopetas");

                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(VenderActivity.this, android.R.layout.simple_spinner_item, spinnerCalibre);
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(VenderActivity.this, android.R.layout.simple_spinner_item, spinnerCategoria);

                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                calibre.setAdapter(adapter1);
                categoria.setAdapter(adapter2);

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                Toast.makeText(VenderActivity.this, "ok", Toast.LENGTH_SHORT).show();
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        descripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VenderActivity.this, DescripcionActivity.class);
                intent.putExtra("texto", "");
                intent.putExtra("editable", true);
                startActivity(intent);
            }
        });

        pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_opciones, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final CheckBox checkBox1 = v.findViewById(R.id.checkBox1);
                final CheckBox checkBox2 = v.findViewById(R.id.checkBox2);

                dialogTitulo.setText("Métodos de Pago");
                checkBox1.setText("Efectivo");
                checkBox2.setText("Todas las Tarjetas (Comisión: 8%)");
                checkBox1.setChecked(true);

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        publicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(VenderActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_opciones, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(VenderActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final CheckBox checkBox1 = v.findViewById(R.id.checkBox1);
                final CheckBox checkBox2 = v.findViewById(R.id.checkBox2);

                dialogTitulo.setText("Tipo de Publicación");
                checkBox1.setText("Regular - S/ 0.00");
                checkBox2.setText("Premium - S/ 10.00 mensuales");
                checkBox1.setChecked(true);

                checkBox1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkBox1.setChecked(true);
                        checkBox2.setChecked(false);
                    }
                });

                checkBox2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkBox1.setChecked(false);
                        checkBox2.setChecked(true);
                    }
                });

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void reload(String id) {
        int idFoto = Integer.parseInt(id);
        for (int i = 0; i < fotos.size(); i++) {
            if (fotos.get(i).getId() == idFoto) {
                fotos.remove(i);
                break;
            }
        }
        adapterFotos.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            final Uri imageUri = data.getData();
            String pt = getRealPathFromURI(VenderActivity.this, imageUri);
            /*try {
                String destinationPath = Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.folder) + "/Perfiles/" + fname;
                File destination = new File(destinationPath);
                try {
                    FileUtils.copyFile(new File(""), destination);
                    path = destination;
                    copy = new File(Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.folder) + "/Clientes/" + fname);
                }catch (IOException e){
                    e.printStackTrace();
                }

                File outputDir = VenderActivity.this.getCacheDir();
                File outputFile = File.createTempFile("temp", ".jpg", outputDir);
            }catch (Exception e) {}*/
            //File path = new File(pt);
            fotos.add(new Fotos(fotos.size() + 1, pt));
            adapterFotos.notifyDataSetChanged();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int check = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (check == PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
        } else {
            Toast.makeText(this, "Necesitamos permisos para poder acceder a la galería", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
