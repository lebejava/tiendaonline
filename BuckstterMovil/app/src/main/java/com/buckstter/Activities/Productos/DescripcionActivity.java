package com.buckstter.Activities.Productos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.buckstter.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class DescripcionActivity extends AppCompatActivity {

    String texto;
    boolean editable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView descripcion = findViewById(R.id.descripcion);
        final TextInputLayout editarBox = findViewById(R.id.editarBox);
        final TextInputEditText editar = findViewById(R.id.editar);
        Button guardar = findViewById(R.id.guardar);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                texto = "";
                editable = false;
            } else {
                texto = extras.getString("texto");
                editable = extras.getBoolean("editable");
            }
        } else {
            texto = (String) savedInstanceState.getSerializable("texto");
            editable = (boolean) savedInstanceState.getSerializable("editable");
        }

        if(editable) {
            descripcion.setVisibility(View.GONE);
            editarBox.setVisibility(View.VISIBLE);
            guardar.setVisibility(View.VISIBLE);
            editar.setText(texto);
        }else{
            descripcion.setVisibility(View.VISIBLE);
            editarBox.setVisibility(View.GONE);
            guardar.setVisibility(View.GONE);
            descripcion.setText(texto);
        }
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String desc = editar.getText().toString();
                Toast.makeText(view.getContext(), desc, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
