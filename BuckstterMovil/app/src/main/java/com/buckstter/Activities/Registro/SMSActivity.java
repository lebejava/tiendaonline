package com.buckstter.Activities.Registro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.buckstter.BroadcastReceiver.SMSReceiver;
import com.buckstter.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SMSActivity extends AppCompatActivity {

    EditText c1, c2, c3, c4;
    TextView temporizador;
    Button cancelar;

    private static final int SMS_PERMISSION_CODE = 0;
    static final String SMS_SERVICE = "android.provider.Telephony.SMS_RECEIVED";
    SMSReceiver receiver;

    String numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        numero = getIntent().getStringExtra("NUMERO");

        receiver = new SMSReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                    Bundle bundle = intent.getExtras();
                    if (bundle != null) {
                        Object[] pdus = (Object[]) bundle.get("pdus");
                        if (pdus.length == 0) {
                            return;
                        }
                        SmsMessage[] messages = new SmsMessage[pdus.length];
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < pdus.length; i++) {
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                            sb.append(messages[i].getMessageBody());
                        }
                        //String sender = messages[0].getOriginatingAddress();
                        String message = sb.toString();
                        if(message.contains("Tu codigo de verificacion de Buckstter es: ")) {
                            String payload = message.substring(message.indexOf(":") + 2, message.indexOf("."));
                            c1.setText(payload.substring(0, 1));
                            c2.setText(payload.substring(1, 2));
                            c3.setText(payload.substring(2, 3));
                            c4.setText(payload.substring(3, 4));
                        }
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter(SMS_SERVICE);
        this.registerReceiver(receiver, filter);

        if (!hasReadSmsPermission()) {
            showRequestPermissionsInfoAlertDialog();
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, getString(R.string.server) + "/numero",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        //response
                        //Log.e("ok", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(SMSActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("numero", numero);
                return params;
            }
        };
        queue.add(postRequest);

        c1 = findViewById(R.id.c1);
        c2 = findViewById(R.id.c2);
        c3 = findViewById(R.id.c3);
        c4 = findViewById(R.id.c4);
        temporizador = findViewById(R.id.temporizador);
        cancelar = findViewById(R.id.cancelar);

        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                long segundos = (millisUntilFinished / 1000);
                if(segundos < 10) {
                    temporizador.setText("00:0" + segundos);
                }else{
                    temporizador.setText("00:" + segundos);
                }
            }

            public void onFinish() {
                //Volver a consultar
                temporizador.setText("00:00");
            }

        }.start();

        c1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(c1.getText().length() == 1 && c2.getText().length() == 1 && c3.getText().length() == 1 && c4.getText().length() == 1) {
                    Verificar();
                }else if(c1.getText().length() == 1) {
                    c2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        c2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(c1.getText().length() == 1 && c2.getText().length() == 1 && c3.getText().length() == 1 && c4.getText().length() == 1) {
                    Verificar();
                }else if(c2.getText().length() == 1) {
                    c3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        c3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(c1.getText().length() == 1 && c2.getText().length() == 1 && c3.getText().length() == 1 && c4.getText().length() == 1) {
                    Verificar();
                }else if(c3.getText().length() == 1) {
                    c4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        c4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(c1.getText().length() == 1 && c2.getText().length() == 1 && c3.getText().length() == 1 && c4.getText().length() == 1) {
                    Verificar();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SMSActivity.this.finish();
            }
        });
    }

    void Verificar() {
        OcultarTeclado();
        final ProgressDialog progressDialog = new ProgressDialog(SMSActivity.this);
        progressDialog.setTitle("Verificando");
        progressDialog.setMessage("Por favor, espere...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, getString(R.string.server) + "/sms",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        //response
                        //Log.e("ok", response);
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                            if(obj.getString("msg").equals("ok")) {
                                Intent intent = new Intent(SMSActivity.this, VerificacionActivity.class);
                                intent.putExtra("TOKEN", obj.getString("data"));
                                startActivity(intent);
                            }else{
                                c1.setText("");
                                c2.setText("");
                                c3.setText("");
                                c4.setText("");
                                c1.requestFocus();
                                Toast.makeText(SMSActivity.this, "El código ingresado es incorrecto. Vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(SMSActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(SMSActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                String sms = c1.getText().toString() + c2.getText().toString() + c3.getText().toString() + c4.getText().toString();
                Map<String, String>  params = new HashMap<String, String>();
                params.put("numero", numero);
                params.put("sms", sms);
                return params;
            }
        };
        queue.add(postRequest);
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                progressDialog.dismiss();
                startActivity(new Intent(SMSActivity.this, VerificacionActivity.class));
            }
        }, 2000);*/
    }

    public void OcultarTeclado() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(SMSActivity.this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(SMSActivity.this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    private void showRequestPermissionsInfoAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permiso de SMS");//R.string.permission_alert_dialog_title
        builder.setMessage("Necesitamos poder leer tus SMS");//R.string.permission_dialog_message
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {//R.string.action_ok
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                requestReadAndSendSmsPermission();
            }
        });
        builder.show();
    }

    private void requestReadAndSendSmsPermission() {
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this, Manifest.permission.READ_SMS)) {
            return;
        }*/
        ActivityCompat.requestPermissions(SMSActivity.this, new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                SMS_PERMISSION_CODE);
    }
}
