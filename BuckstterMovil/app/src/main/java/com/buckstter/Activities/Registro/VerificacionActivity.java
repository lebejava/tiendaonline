package com.buckstter.Activities.Registro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.buckstter.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class VerificacionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificacion);
        FloatingActionButton qr = findViewById(R.id.qr);
        TextView noLicense = findViewById(R.id.noLicense);

        final String token = getIntent().getStringExtra("TOKEN");

        qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerificacionActivity.this, QRActivity.class);
                intent.putExtra("TOKEN", token);
                startActivity(intent);
            }
        });

        noLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                builder.setTitle("Aviso")
                        .setMessage("Si no escaneas tu licencia, no podrás comprar o vender armas de fuego y/o municiones.")
                        .setPositiveButton("ACEPTO",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(VerificacionActivity.this, "Next", Toast.LENGTH_SHORT).show();
                                    }
                                })
                        .setNegativeButton("CANCELAR",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                builder.create();
                builder.show();
            }
        });
    }
}
