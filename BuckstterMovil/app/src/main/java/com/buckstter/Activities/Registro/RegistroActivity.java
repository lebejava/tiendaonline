package com.buckstter.Activities.Registro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.buckstter.MainActivity;
import com.buckstter.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {

    String token;
    TextView nombre, documento, licencia, vencimiento;
    Button finalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombre = findViewById(R.id.nombre);
        documento = findViewById(R.id.documento);
        licencia = findViewById(R.id.licencia);
        vencimiento = findViewById(R.id.vencimiento);
        finalizar = findViewById(R.id.finalizar);

        token = getIntent().getStringExtra("TOKEN");
        nombre.setText(getIntent().getStringExtra("NOMBRE"));
        documento.setText(getIntent().getStringExtra("DOCUMENTO"));
        licencia.setText(getIntent().getStringExtra("NUMERO"));
        vencimiento.setText(getIntent().getStringExtra("VENCIMIENTO"));

        String[] modalidades = getIntent().getStringExtra("MODALIDADES").split(",");
        for(int i = 0; i < modalidades.length; i++) {
            String text = licencia.getText().toString() + "\n * " + modalidades[i];
            licencia.setText(text);
        }

        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(RegistroActivity.this)
                        .setTitle("Términos y Condiciones")
                        .setMessage(R.string.terminos_y_condiciones)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                RequestQueue queue = Volley.newRequestQueue(RegistroActivity.this);
                                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, getString(R.string.server) + "/api/activar", (JSONObject) null,
                                        new Response.Listener<JSONObject>()
                                        {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    if(response.getString("msg").equals("ok")) {
                                                        token = response.getString("data");
                                                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                                                        intent.putExtra("TOKEN", token);
                                                        startActivity(intent);
                                                    }else{
                                                        Toast.makeText(RegistroActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(RegistroActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener()
                                        {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Toast.makeText(RegistroActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                ) {
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("auth", "bearer " + token);
                                        return headers;
                                    }
                                };
                                queue.add(getRequest);
                            }
                        })
                        .setNegativeButton("RECHAZAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                TextView textView = dialog.findViewById(android.R.id.message);
                textView.setMaxLines(10);
                textView.setScroller(new Scroller(RegistroActivity.this));
                textView.setVerticalScrollBarEnabled(true);
                textView.setMovementMethod(new ScrollingMovementMethod());
            }
        });
    }
}
