package com.buckstter.Activities.MiCuenta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.buckstter.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class InformacionPersonalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_personal);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CardView direccionBox = findViewById(R.id.direccionBox);
        TextView direccion = findViewById(R.id.direccion);
        direccionBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(InformacionPersonalActivity.this);
                View v = layoutInflaterAndroid.inflate(R.layout.dialog_input, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(InformacionPersonalActivity.this);
                alertDialogBuilderUserInput.setView(v);

                final TextView dialogTitulo = v.findViewById(R.id.titulo);
                final TextInputLayout dialogLayout = v.findViewById(R.id.textoLayout);
                final TextInputEditText dialogTexto = v.findViewById(R.id.texto);

                dialogTitulo.setText("Ingresa tu Domicilio");
                dialogLayout.setHint("Dirección");

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int in) {
                                Toast.makeText(InformacionPersonalActivity.this, dialogTexto.getText().toString(), Toast.LENGTH_SHORT).show();
                                dialogBox.dismiss();
                            }
                        })

                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
