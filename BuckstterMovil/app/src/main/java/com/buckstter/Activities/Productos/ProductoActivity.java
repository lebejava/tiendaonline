package com.buckstter.Activities.Productos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.buckstter.Activities.MiCuenta.CategoriasActivity;
import com.buckstter.Adapter.AdapterProductosVertical;
import com.buckstter.Adapter.FunctionCategorias;
import com.buckstter.Objetos.Productos;
import com.buckstter.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

public class ProductoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        final int[] images = {R.drawable.imagen1, R.drawable.imagen2, R.drawable.imagen3};
        final CarouselView carousel = findViewById(R.id.carousel);
        CardView descripcion = findViewById(R.id.descripcion);
        carousel.setPageCount(images.length);
        carousel.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(images[position]);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        });
        descripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductoActivity.this, DescripcionActivity.class);
                intent.putExtra("texto", "");
                intent.putExtra("editable", false);
                startActivity(intent);
            }
        });

        RecyclerView recomendadosList = findViewById(R.id.recomendadosList);

        recomendadosList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        List<Productos> recomendados = new ArrayList<>();
        AdapterProductosVertical adapterProductosRecientes = new AdapterProductosVertical(this, recomendados);
        recomendadosList.setAdapter(adapterProductosRecientes);
        recomendados.clear();
        recomendadosList.setFocusable(false);

        recomendados.add(new Productos(1, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));
        recomendados.add(new Productos(2, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));
        recomendados.add(new Productos(3, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));

        adapterProductosRecientes.notifyDataSetChanged();
    }
}
