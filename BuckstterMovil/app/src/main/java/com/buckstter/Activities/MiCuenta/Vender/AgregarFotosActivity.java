package com.buckstter.Activities.MiCuenta.Vender;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.buckstter.Adapter.Function;
import com.buckstter.Adapter.MapComparator;
import com.buckstter.R;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class AgregarFotosActivity extends AppCompatActivity {

    GridView galeria_grid;
    ArrayList<HashMap<String, String>> imageList = new ArrayList<>();
    LoadAlbumImages loadAlbumTask;
    Boolean primera = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_fotos);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        galeria_grid = findViewById(R.id.galeria_grid);
        int iDisplayWidth = getResources().getDisplayMetrics().widthPixels;
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = iDisplayWidth / (metrics.densityDpi / 160f);

        if (dp < 360) {
            dp = (dp - 17) / 2;
            float px = Function.convertDpToPixel(dp, this);
            galeria_grid.setColumnWidth(Math.round(px));
        }

        int check = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (check != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AgregarFotosActivity.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1024);
        }else{
            loadAlbumTask = new AgregarFotosActivity.LoadAlbumImages();
            loadAlbumTask.execute();
            primera = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (primera != null && !primera) {
            loadAlbumTask = new AgregarFotosActivity.LoadAlbumImages();
            loadAlbumTask.execute();
        } else if(primera != null) {
            primera = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    class LoadAlbumImages extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            imageList.clear();
        }

        protected String doInBackground(String... args) {
            String xml = "";
            Uri uri;
            Cursor cursor;
            int column_index_data, column_index_folder_name;
            ArrayList<String> listOfAllImages = new ArrayList<String>();
            String absolutePathOfImage = null;
            uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

            String[] projection = { MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

            cursor = getContentResolver().query(uri, projection, null,
                    null, null);

            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            column_index_folder_name = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data);
                imageList.add(Function.mappingInbox("1", "name", "deskription", absolutePathOfImage, "0", Function.converToTime("0")));

                listOfAllImages.add(absolutePathOfImage);
            }

            /*AdminSQLite admin = new AdminSQLite(getActivity(), "datos", null, 1);
            SQLiteDatabase db = admin.getWritableDatabase();
            String[] col = new String[]{"id", "path", "nombre", "descripcion"};
            Cursor fi = db.query("galeria", col, null, null, null, null, null);
            if (fi != null) {
                while (fi.moveToNext()) {
                    File mypath = null;
                    if (!fi.getString(1).isEmpty()) {
                        mypath = new File(Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.folder) + "/Galeria/" + fi.getString(1));
                        imageList.add(Function.mappingInbox(fi.getString(0), fi.getString(2), fi.getString(3), mypath.toString(), String.valueOf(mypath.lastModified()), Function.converToTime(String.valueOf(mypath.lastModified()))));
                    } else {
                        imageList.add(Function.mappingInbox(fi.getString(0), fi.getString(2), fi.getString(3), "", "0", Function.converToTime("0")));
                    }
                }
            }
            db.close();*/
            Collections.sort(imageList, new MapComparator(Function.KEY_TIMESTAMP, "dsc"));
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {
            final boolean[] long_click = {false};

            SingleAlbumAdapter adapter = new SingleAlbumAdapter(AgregarFotosActivity.this, imageList);
            galeria_grid.setAdapter(adapter);
            galeria_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        final int position, long id) {
                    if (!long_click[0]) {
                        if (imageList.get(+position).get(Function.KEY_PATH).isEmpty()) {
                            Toast.makeText(view.getContext(), "Este registro no tiene imagen", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AgregarFotosActivity.this, "Acción", Toast.LENGTH_SHORT).show();
                            /*Intent intent = new Intent(AgregarFotosActivity.this, VisualizadorActivity.class);
                            intent.putExtra("path", imageList.get(+position).get(Function.KEY_PATH));
                            intent.putExtra("nombre", imageList.get(+position).get(Function.KEY_NOMBRE));
                            intent.putExtra("descripcion", imageList.get(+position).get(Function.KEY_DESCRIPCION));
                            startActivity(intent);*/
                        }
                    }
                }
            });
            galeria_grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    long_click[0] = true;
                    AlertDialog.Builder builder = new AlertDialog.Builder(AgregarFotosActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("Eliminar");
                    builder.setMessage("¿Deseas eliminar este elemento?");
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    /*AdminSQLite admin = new AdminSQLite(getActivity(), "datos", null, 1);
                                    SQLiteDatabase db = admin.getWritableDatabase();
                                    File fileDelete = new File(imageList.get(+position).get(Function.KEY_PATH));
                                    fileDelete.delete();
                                    db.delete("galeria", "id = " + imageList.get(+position).get(Function.KEY_ID), null);
                                    db.close();*/
                                    long_click[0] = false;
                                    onResume();
                                }
                            });
                    builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            long_click[0] = false;
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int check = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (check == PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            loadAlbumTask = new AgregarFotosActivity.LoadAlbumImages();
            loadAlbumTask.execute();
            primera = true;
        }else{
            Toast.makeText(this, "Necesitamos permisos para poder acceder a los archivos y mostrarlos", Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }
}

class SingleAlbumAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;

    public SingleAlbumAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data = d;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SingleAlbumViewHolder holder = null;
        if (convertView == null) {
            holder = new SingleAlbumViewHolder();
            convertView = LayoutInflater.from(activity).inflate(
                    R.layout.card_foto, parent, false);

            holder.galleryImage = convertView.findViewById(R.id.galleryImage);
            holder.gallery_nombre = convertView.findViewById(R.id.gallery_nombre);

            convertView.setTag(holder);
        } else {
            holder = (SingleAlbumViewHolder) convertView.getTag();
        }
        holder.galleryImage.setId(position);
        holder.gallery_nombre.setId(position);

        HashMap<String, String> song;
        song = data.get(position);
        try {
            holder.gallery_nombre.setText(song.get(Function.KEY_NOMBRE));

            Glide.with(activity)
                    .load(new File(song.get(Function.KEY_PATH)))
                    .into(holder.galleryImage);

        } catch (Exception e) {
        }

        return convertView;
    }
}

class SingleAlbumViewHolder {
    ImageView galleryImage;
    TextView gallery_nombre;
}
