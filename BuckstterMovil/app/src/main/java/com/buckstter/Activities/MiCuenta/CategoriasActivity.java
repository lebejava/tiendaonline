package com.buckstter.Activities.MiCuenta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.buckstter.Activities.Productos.ResultadosActivity;
import com.buckstter.Adapter.FunctionCategorias;
import com.buckstter.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoriasActivity extends AppCompatActivity {

    GridView galeria_grid;
    ArrayList<HashMap<String, String>> imageList = new ArrayList<>();
    LoadAlbumImages loadAlbumTask;
    boolean primera = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        galeria_grid = findViewById(R.id.galeria_grid);

        loadAlbumTask = new LoadAlbumImages();
        loadAlbumTask.execute();
        primera = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!primera) {
            loadAlbumTask = new LoadAlbumImages();
            loadAlbumTask.execute();
        } else {
            primera = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    class LoadAlbumImages extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            imageList.clear();
        }

        protected String doInBackground(String... args) {
            String xml = "";
            imageList.add(FunctionCategorias.mappingInbox("1", "Pistolas"));
            imageList.add(FunctionCategorias.mappingInbox("2", "Revólveres"));
            imageList.add(FunctionCategorias.mappingInbox("3", "Carabinas"));
            imageList.add(FunctionCategorias.mappingInbox("4", "Escopetas"));
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {
            SingleAlbumAdapter adapter = new SingleAlbumAdapter(CategoriasActivity.this, imageList);
            galeria_grid.setAdapter(adapter);
            galeria_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        final int position, long id) {
                    Intent intent = new Intent(CategoriasActivity.this, ResultadosActivity.class);
                    intent.putExtra("categoria", imageList.get(+position).get(FunctionCategorias.KEY_NOMBRE));
                    startActivity(intent);
                }
            });
        }
    }
}

class SingleAlbumAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;

    public SingleAlbumAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data = d;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SingleAlbumViewHolder holder = null;
        if (convertView == null) {
            holder = new SingleAlbumViewHolder();
            convertView = LayoutInflater.from(activity).inflate(
                    R.layout.card_categoria, parent, false);

            holder.galleryImage = convertView.findViewById(R.id.galleryImage);
            holder.gallery_nombre = convertView.findViewById(R.id.gallery_nombre);

            convertView.setTag(holder);
        } else {
            holder = (SingleAlbumViewHolder) convertView.getTag();
        }
        holder.galleryImage.setId(position);
        holder.gallery_nombre.setId(position);

        HashMap<String, String> song;
        song = data.get(position);
        try {
            holder.gallery_nombre.setText(song.get(FunctionCategorias.KEY_NOMBRE));
            String id = song.get(FunctionCategorias.KEY_ID);
            if(id.equals("1")) {
                holder.galleryImage.setImageResource(R.drawable.pistolas);
            }else if(id.equals("2")) {
                holder.galleryImage.setImageResource(R.drawable.revolveres);
            }else if(id.equals("3")) {
                holder.galleryImage.setImageResource(R.drawable.carabinas);
            }else if(id.equals("4")) {
                holder.galleryImage.setImageResource(R.drawable.escopetas);
            }
        } catch (Exception e) {
        }

        return convertView;
    }
}

class SingleAlbumViewHolder {
    ImageView galleryImage;
    TextView gallery_nombre;
}
