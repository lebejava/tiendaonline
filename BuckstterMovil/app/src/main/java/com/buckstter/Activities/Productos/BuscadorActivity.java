package com.buckstter.Activities.Productos;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;

import com.buckstter.Adapter.AdapterBuscador;
import com.buckstter.Adapter.AdapterProductos;
import com.buckstter.Objetos.Productos;
import com.buckstter.Objetos.Buscador;
import com.buckstter.R;

import java.util.ArrayList;
import java.util.List;

public class BuscadorActivity extends AppCompatActivity {

    RecyclerView resultadosList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscador);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resultadosList = findViewById(R.id.resultadosList);

        resultadosList.setLayoutManager(new LinearLayoutManager(this));
        List<Buscador> resultados = new ArrayList<>();
        AdapterBuscador adapterBuscador = new AdapterBuscador(this, resultados);
        resultadosList.setAdapter(adapterBuscador);
        resultados.clear();
        resultadosList.setFocusable(false);

        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));
        resultados.add(new Buscador("Pistolas"));

        adapterBuscador.notifyDataSetChanged();

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Intent intent = new Intent(BuscadorActivity.this, ResultadosActivity.class);
            startActivity(intent);
            return true;
        }

        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
