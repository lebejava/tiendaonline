package com.buckstter.Activities.Registro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.buckstter.R;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},1);
            }
        }

        token = getIntent().getStringExtra("TOKEN");

        ViewGroup contentFrame = findViewById(R.id.content_qr);
        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(final Result rawResult) {

        final String url = rawResult.getText();
        final ProgressDialog progressDialog = new ProgressDialog(QRActivity.this);
        progressDialog.setTitle("Verificando");
        progressDialog.setMessage("Por favor, espere...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, getString(R.string.server) + "/api/qr",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        //response
                        //Log.e("ok", response);
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                            if(obj.getString("msg").equals("ok")) {
                                JSONObject data = obj.getJSONObject("data");
                                token = data.getString("token");
                                Intent intent = new Intent(QRActivity.this, RegistroActivity.class);
                                intent.putExtra("TOKEN", token);
                                intent.putExtra("NOMBRE", data.getString("nombre"));
                                intent.putExtra("DOCUMENTO", data.getString("documento"));
                                intent.putExtra("NUMERO", data.getString("numero"));
                                intent.putExtra("VENCIMIENTO", data.getString("vencimiento"));
                                intent.putExtra("MODALIDADES", data.getString("modalidades"));
                                startActivity(intent);
                            }else{
                                Toast.makeText(QRActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(QRActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        progressDialog.dismiss();
                        Toast.makeText(QRActivity.this, "Hubo un error. Revisa tu conexión y vuelve a intentarlo más tarde.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("auth", "bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("url", url);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private static class CustomViewFinderView extends ViewFinderView {
        public CustomViewFinderView(Context context) {
            super(context);
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            setSquareViewFinder(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
        }
    }
}
