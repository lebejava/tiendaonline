package com.buckstter.Activities.Productos;

import android.os.Bundle;

import com.buckstter.Adapter.AdapterProductos;
import com.buckstter.Objetos.Productos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.buckstter.R;

import java.util.ArrayList;
import java.util.List;

public class ResultadosActivity extends AppCompatActivity {

    RecyclerView resultadosList;
    String categoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Hola");
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResultadosActivity.this.onBackPressed();
            }
        });

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                categoria = null;
            } else {
                categoria = extras.getString("categoria");
            }
        } else {
            categoria = (String) savedInstanceState.getSerializable("categoria");
        }

        if(categoria != null) {
            getSupportActionBar().setTitle(categoria);
        }

        resultadosList = findViewById(R.id.resultadosList);

        resultadosList.setLayoutManager(new LinearLayoutManager(this));
        List<Productos> todos = new ArrayList<>();
        AdapterProductos adapterProductosTodos = new AdapterProductos(this, todos);
        resultadosList.setAdapter(adapterProductosTodos);
        todos.clear();
        resultadosList.setFocusable(false);

        todos.add(new Productos(1, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        todos.add(new Productos(2, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        todos.add(new Productos(3, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));

        adapterProductosTodos.notifyDataSetChanged();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
