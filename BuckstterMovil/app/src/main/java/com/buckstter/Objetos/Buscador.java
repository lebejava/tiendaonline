package com.buckstter.Objetos;

public class Buscador {
    String resultado;

    public Buscador() {
    }

    public Buscador(String resultado) {
        this.resultado = resultado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
