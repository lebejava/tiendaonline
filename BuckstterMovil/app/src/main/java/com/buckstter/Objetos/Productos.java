package com.buckstter.Objetos;

public class Productos {
    int id;
    String imagen, tipoImagen, titulo, precio, estado;

    public Productos() {
    }

    public Productos(int id, String imagen, String tipoImagen, String titulo, String precio, String estado) {
        this.id = id;
        this.imagen = imagen;
        this.tipoImagen = tipoImagen;
        this.titulo = titulo;
        this.precio = precio;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTipoImagen() {
        return tipoImagen;
    }

    public void setTipoImagen(String tipoImagen) {
        this.tipoImagen = tipoImagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
