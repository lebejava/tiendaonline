package com.buckstter.Objetos;

public class Novedades {
    int id;
    String imagen, url;

    public Novedades() {
    }

    public Novedades(int id, String imagen, String url) {
        this.id = id;
        this.imagen = imagen;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
