package com.buckstter.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.buckstter.Objetos.Buscador;
import com.buckstter.R;

import java.util.List;

public class AdapterBuscador extends RecyclerView.Adapter<AdapterBuscador.viewHolder>{

    List<Buscador> buscador;
    private static Context context;
    private ImageLoader mImageLoader;

    public AdapterBuscador(Context context, List<Buscador> buscador) {
        this.context = context;
        this.buscador = buscador;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_historial_busqueda, parent, false);
        viewHolder holder = new viewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        Buscador p = buscador.get(position);
        holder.texto.setText(p.getResultado());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Go", Toast.LENGTH_SHORT).show();
                //view.getContext().startActivity(new Intent(context, ProductoActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return buscador.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView texto;

        public viewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            texto = itemView.findViewById(R.id.texto);
        }
    }

}
