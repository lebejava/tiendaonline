package com.buckstter.Adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.buckstter.Activities.Productos.ProductoActivity;
import com.buckstter.ImageRequest.ImageRequestQueue;
import com.buckstter.Objetos.Productos;
import com.buckstter.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class AdapterProductos extends RecyclerView.Adapter<AdapterProductos.viewHolder>{

    List<Productos> productos;
    private static Context context;
    private ImageLoader mImageLoader;

    public AdapterProductos(Context context, List<Productos> productos) {
        this.context = context;
        this.productos = productos;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_producto, parent, false);
        viewHolder holder = new viewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        Productos p = productos.get(position);
        holder.id.setText(String.valueOf(p.getId()));

        mImageLoader = ImageRequestQueue.getInstance(context)
                .getImageLoader();
        mImageLoader.get(p.getImagen(), ImageLoader.getImageListener(holder.imagen,
                R.drawable.preview, android.R.drawable
                        .ic_dialog_alert));
        holder.imagen.setImageUrl(p.getImagen(), mImageLoader);
        holder.titulo.setText(p.getTitulo());
        holder.precio.setText(p.getPrecio());
        holder.estado.setText(p.getEstado());
        holder.agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.agregar.setImageResource(R.drawable.ic_favorite);
                Toast.makeText(v.getContext(), "Carrito: " + holder.id.getText().toString(), Toast.LENGTH_SHORT).show();
                /*if(Integer.parseInt(holder.count.getText().toString()) < 1000) {
                    ((MainActivity) context).AgregarProducto(holder.id.getText().toString());
                    notifyDataSetChanged();
                }*/
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(context, ProductoActivity.class));
            }
        });
        /*holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), holder.precio.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), holder.precio.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        NetworkImageView imagen;
        TextView id, titulo, precio, estado;
        FloatingActionButton agregar;

        public viewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            id = itemView.findViewById(R.id.id);
            imagen = itemView.findViewById(R.id.imagen);
            titulo = itemView.findViewById(R.id.titulo);
            precio = itemView.findViewById(R.id.precio);
            estado = itemView.findViewById(R.id.estado);
            agregar = itemView.findViewById(R.id.agregar);
        }
    }

}
