package com.buckstter.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.buckstter.ImageRequest.ImageRequestQueue;
import com.buckstter.Objetos.Novedades;
import com.buckstter.R;

import java.util.List;


public class AdapterNovedades extends RecyclerView.Adapter<AdapterNovedades.viewHolder>{

    List<Novedades> novedades;
    private static Context context;
    private ImageLoader mImageLoader;

    public AdapterNovedades(Context context, List<Novedades> novedades) {
        this.context = context;
        this.novedades = novedades;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_novedades, parent, false);
        viewHolder holder = new viewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        final Novedades p = novedades.get(position);
        holder.id.setText(String.valueOf(p.getId()));

        mImageLoader = ImageRequestQueue.getInstance(context)
                .getImageLoader();
        mImageLoader.get(p.getImagen(), ImageLoader.getImageListener(holder.imagen,
                R.drawable.preview, android.R.drawable
                        .ic_dialog_alert));
        holder.imagen.setImageUrl(p.getImagen(), mImageLoader);
        holder.imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), p.getUrl(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return novedades.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        NetworkImageView imagen;
        TextView id;

        public viewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            id = itemView.findViewById(R.id.id);
            imagen = itemView.findViewById(R.id.imagen);
        }
    }

}
