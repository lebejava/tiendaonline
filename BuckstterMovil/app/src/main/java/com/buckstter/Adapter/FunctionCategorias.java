package com.buckstter.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class FunctionCategorias {

    public static final String KEY_ID = "0";
    public static final String KEY_NOMBRE = "";

    public static HashMap<String, String> mappingInbox(String id, String nombre) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(KEY_ID, id);
        map.put(KEY_NOMBRE, nombre);
        return map;
    }

}
