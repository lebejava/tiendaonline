package com.buckstter.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.buckstter.ImageRequest.ImageRequestQueue;
import com.buckstter.Objetos.Fotos;
import com.buckstter.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.EventListener;
import java.util.List;

public class AdapterFotos extends RecyclerView.Adapter<AdapterFotos.viewHolder> {

    EventListener eliminar;
    List<Fotos> fotos;
    private static Context context;

    public interface EventListener {
        void reload(String path);
    }

    public AdapterFotos(Context context, List<Fotos> fotos, EventListener eliminar) {
        this.context = context;
        this.fotos = fotos;
        this.eliminar = eliminar;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_foto, parent, false);
        viewHolder holder = new viewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        Fotos p = fotos.get(position);
        holder.id.setText(String.valueOf(p.getId()));

        if (position == 0) {
            holder.portada.setVisibility(View.VISIBLE);
        } else {
            holder.portada.setVisibility(View.GONE);
        }

        if (p.getUrl().contains("http")) {
            Picasso.get().load(p.getUrl()).into(holder.imagen);
        } else {
            Glide.with(context)
                    .load(new File(p.getUrl()))
                    .apply(RequestOptions.noTransformation())
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(holder.imagen);
        }
        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                builder.setTitle("Eliminar");
                builder.setMessage("¿Deseas eliminar este elemento?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                eliminar.reload(holder.id.getText().toString());
                            }
                        });
                builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return fotos.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView id;
        ImageView imagen;
        LinearLayout portada;

        public viewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            id = itemView.findViewById(R.id.id);
            imagen = itemView.findViewById(R.id.imagen);
            portada = itemView.findViewById(R.id.portada);
        }
    }

}
