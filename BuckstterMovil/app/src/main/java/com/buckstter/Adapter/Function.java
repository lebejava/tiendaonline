package com.buckstter.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Function {

    public static final String KEY_ID = "0";
    public static final String KEY_NOMBRE = "Nombre";
    public static final String KEY_DESCRIPCION = "Descripcion";
    public static final String KEY_PATH = "path";
    public static final String KEY_TIMESTAMP = "timestamp";
    static final String KEY_TIME = "date";

    public static HashMap<String, String> mappingInbox(String id, String nombre, String descripcion, String path, String timestamp, String time) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(KEY_ID, id);
        map.put(KEY_NOMBRE, nombre);
        map.put(KEY_DESCRIPCION, descripcion);
        map.put(KEY_PATH, path);
        map.put(KEY_TIMESTAMP, timestamp);
        map.put(KEY_TIME, time);
        return map;
    }


    public static String converToTime(String timestamp) {
        long datetime = Long.parseLong(timestamp);
        Date date = new Date(datetime);
        DateFormat formatter = new SimpleDateFormat("dd/MM HH:mm");
        return formatter.format(date);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

}
