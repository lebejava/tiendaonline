package com.buckstter.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.buckstter.Adapter.AdapterNovedades;
import com.buckstter.Adapter.AdapterProductos;
import com.buckstter.Adapter.AdapterProductosVertical;
import com.buckstter.ImageRequest.ImageRequestQueue;
import com.buckstter.Objetos.Novedades;
import com.buckstter.Objetos.Productos;
import com.buckstter.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InicioFragment extends Fragment {

    CarouselView carousel;
    RecyclerView recientesList, recomendadosList, productosList;
    NetworkImageView vender;
    private ImageLoader mImageLoader;

    public InicioFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_inicio, container, false);
        //novedadesList = v.findViewById(R.id.novedades);
        carousel = v.findViewById(R.id.carousel);
        recientesList = v.findViewById(R.id.recientes);
        recomendadosList = v.findViewById(R.id.recomendados);
        vender = v.findViewById(R.id.vender);
        productosList = v.findViewById(R.id.productosList);

        final int[] images = {R.drawable.clay, R.drawable.cyber};
        carousel.setPageCount(images.length);
        carousel.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(images[position]);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });
        /*novedadesList.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Novedades> novedades = new ArrayList<>();
        AdapterNovedades adapterNovedades = new AdapterNovedades(v.getContext(), novedades);
        novedadesList.setAdapter(adapterNovedades);
        novedades.clear();
        novedadesList.setFocusable(false);

        novedades.add(new Novedades(1, "https://www.es-commerce.com/imagenes/Consejos-para-tiendas-de-alimentacion-online.jpg", "http://ofertas.com"));
        novedades.add(new Novedades(2, "https://www.es-commerce.com/imagenes/Consejos-para-tiendas-de-alimentacion-online.jpg", "http://ofertas.com"));
        novedades.add(new Novedades(3, "https://www.es-commerce.com/imagenes/Consejos-para-tiendas-de-alimentacion-online.jpg", "http://ofertas.com"));

        adapterNovedades.notifyDataSetChanged();*/

        recientesList.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Productos> recientes = new ArrayList<>();
        AdapterProductosVertical adapterProductosRecientes = new AdapterProductosVertical(v.getContext(), recientes);
        recientesList.setAdapter(adapterProductosRecientes);
        recientes.clear();
        recientesList.setFocusable(false);

        recientes.add(new Productos(1, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));
        recientes.add(new Productos(2, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));
        recientes.add(new Productos(3, "https://www.budsgunshop.com/catalog/images/719017027.jpg", "Referencial", "Pistola Glock 19 Gen4", "S/ 2.900", "Nuevo"));

        adapterProductosRecientes.notifyDataSetChanged();

        recomendadosList.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Productos> recomendados = new ArrayList<>();
        AdapterProductosVertical adapterProductosRecomendados = new AdapterProductosVertical(v.getContext(), recomendados);
        recomendadosList.setAdapter(adapterProductosRecomendados);
        recomendados.clear();
        recomendadosList.setFocusable(false);

        recomendados.add(new Productos(1, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        recomendados.add(new Productos(2, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        recomendados.add(new Productos(3, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));

        adapterProductosRecomendados.notifyDataSetChanged();

        mImageLoader = ImageRequestQueue.getInstance(v.getContext())
                .getImageLoader();//https://www.marketingandweb.es/wp-content/uploads/2015/06/como-vender-por-internet-tienda-online.png
        mImageLoader.get("https://www.innovapublicidad.es/wp-content/uploads/2016/06/Razones-vender-online-bg.jpg", ImageLoader.getImageListener(vender,
                R.drawable.preview, android.R.drawable
                        .ic_dialog_alert));

        vender.setImageUrl("https://www.innovapublicidad.es/wp-content/uploads/2016/06/Razones-vender-online-bg.jpg", mImageLoader);

        productosList.setLayoutManager(new LinearLayoutManager(v.getContext()));
        List<Productos> todos = new ArrayList<>();
        AdapterProductos adapterProductosTodos = new AdapterProductos(v.getContext(), todos);
        productosList.setAdapter(adapterProductosTodos);
        todos.clear();
        productosList.setFocusable(false);

        todos.add(new Productos(1, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        todos.add(new Productos(2, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));
        todos.add(new Productos(3, "http://www.ultimocartucho.es/wp-content/uploads/2015/03/pistola_beretta_apx_aguja_lanzada_.jpg", "Referencial", "Pistola Beretta APX", "S/ 2.649", "Nuevo"));

        adapterProductosTodos.notifyDataSetChanged();
        return v;
    }

}
