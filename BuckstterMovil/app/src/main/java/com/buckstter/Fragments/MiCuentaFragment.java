package com.buckstter.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.buckstter.Activities.MiCuenta.CategoriasActivity;
import com.buckstter.Activities.MiCuenta.InformacionPersonalActivity;
import com.buckstter.Activities.MiCuenta.VenderActivity;
import com.buckstter.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MiCuentaFragment extends Fragment {

    public MiCuentaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mi_cuenta, container, false);
        CardView categorias = v.findViewById(R.id.categorias);
        CardView vender = v.findViewById(R.id.vender);
        CardView informacion_personal = v.findViewById(R.id.informacion_personal);

        categorias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), CategoriasActivity.class));
            }
        });
        vender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), VenderActivity.class));
            }
        });
        informacion_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), InformacionPersonalActivity.class));
            }
        });
        return v;
    }

}
