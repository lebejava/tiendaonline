package com.buckstter;

import android.content.Intent;
import android.os.Bundle;

import com.buckstter.Activities.Productos.BuscadorActivity;
import com.buckstter.Fragments.FavoritosFragment;
import com.buckstter.Fragments.InicioFragment;
import com.buckstter.Fragments.MiCuentaFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    String token;
    Fragment fragment = null;
    Toolbar toolbar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_inicio:
                    fragment = new InicioFragment();
                    getSupportFragmentManager().
                            beginTransaction().
                            replace(R.id.fragment_content, fragment).
                            commit();
                    toolbar.setTitle("Buckstter");
                    return true;
                case R.id.navigation_favoritos:
                    fragment = new FavoritosFragment();
                    getSupportFragmentManager().
                            beginTransaction().
                            replace(R.id.fragment_content, fragment).
                            commit();
                    toolbar.setTitle("Buckstter");
                    return true;
                case R.id.navigation_mi_cuenta:
                    fragment = new MiCuentaFragment();
                    getSupportFragmentManager().
                            beginTransaction().
                            replace(R.id.fragment_content, fragment).
                            commit();
                    toolbar.setTitle("Buckstter");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BottomNavigationView navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        /*if (getIntent().getStringExtra("TOKEN") == null) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {*/
            token = getIntent().getStringExtra("TOKEN");
            fragment = new InicioFragment();
            getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.fragment_content, fragment).
                    commit();
            toolbar.setTitle("Buckstter");
        //}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
                Intent intent = new Intent(MainActivity.this, BuscadorActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
