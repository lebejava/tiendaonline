var express = require('express')
var app = express()
var http = require('http').Server(app)
var io = require('socket.io')(http)
var jwt = require('jsonwebtoken')
var db = require('mongoose')
var ObjectId = require('mongodb').ObjectID
var cookieParser = require('cookie-parser')
var parser = require('body-parser')
var moment = require('moment')
var request = require('request')
var cheerio = require('cheerio')
var fs = require('fs')
var CronJob = require('cron').CronJob
var config = require('./config.js')

app.set('views', __dirname + '/View')
app.set('view engine', 'ejs')
app.use(express.static(__dirname + '/Public'))
app.set('key', config.secret)
app.use(parser())
app.use(cookieParser())

db.Promise = global.Promise
db.connect(config.database)
var MongoDB = db.connection

const accountSid = config.twilioSID
const authToken = config.twilioTOKEN
const twilio = require('twilio')(accountSid, authToken)

var api = express.Router()

api.use(function(req, res, next) {
  try {
    var token = req.headers.auth.split(' ')[1]
    //var token = req.body.token || req.query.token || req.headers['x-access-token']
    jwt.verify(token, app.get('key'), function(error, decoded) {
      if (error) {
        return res.json({ msg: 'error', data: 'Autorization_error' })
      }else{
        req.token = decoded
        next()
      }
    })
  }catch(e) {
    return res.status(403).json({msg: 'error', data: 'Autorization_required' })
  }
})

app.use('/api', api)

//API REST

//Exportar
app.get('/dolar', function(req, res){
  var compra, venta

  request('http://www.precio-dolar.pe/', function(error, response, html){
    if(!error){
      var $ = cheerio.load(html)

      var json = { moneda : "PEN", compra : "", venta : ""}

      $('.buy').filter(function(){
        var data = $(this)
        compra = data.children().first().text()

        json.compra = compra
      })

      $('.sell').filter(function(){
        var data = $(this)
        venta = data.children().first().text()

        json.venta = venta
      })
      res.json({msg : 'ok', response : json})
    }else{
      res.json({msg : 'error', type : error})
    }
  })
})
//Fin exportar

app.post('/login', function(req, res) {
  var user = req.body.user.trim()
  var pass = req.body.pass.trim()

  var m = moment().hour(0).minute(0).second(0).millisecond(0)
  m = m.add(1, 'days')
  var ms = moment(m).diff(moment())
  var r = moment.duration(ms)
  var restante = Math.floor(r / 1000)

  MongoDB.collection('usuarios').findOne({'numero': user, 'sms': pass}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: error})
    }else if(find) {
      if(find.activo) {
        res.json({msg: 'ok', data: jwt.sign({usuario: find._id, activo: true}, config.secret, { expiresIn: restante })})
      }else{
        res.json({msg: 'error', data: jwt.sign({usuario: find._id, activo: false}, config.secret, { expiresIn: restante })})
      }
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })
})

app.post('/numero', function(req, res) {
  var numero = req.body.numero.trim()
  MongoDB.collection('usuarios').findOne({'numero': numero}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: error})
    }else if(find){
      console.log(find.sms);
      var sms = String(Math.floor(Math.random() * (9999 - 1000) + 1000))
      find.sms = sms
      MongoDB.collection('usuarios').update({'_id': db.Types.ObjectId(find._id)}, find, function(error, updated) {
        if(error) {
          res.json({msg: 'error', data: error})
        }else{
          console.log("Update => Número: " + numero + " | SMS: " + sms)
          res.json({msg: 'ok'})
          /*twilio.messages.create({
            body: 'Tu codigo de verificacion de Buckstter es: ' + sms + '\nNo compartas este codigo con nadie.',
            from: '+12054330484',
            to: '+51' + numero
          }).then(function(message) {
            //console.log(message)
            console.log("Update => Número: " + numero + " | SMS: " + sms)
            res.json({msg: 'ok'})
          })*/
        }
      })
    }else{
      var sms = String(Math.floor(Math.random() * (9999 - 1000) + 1000))
      var data = {
        numero: numero,
        sms: sms,
        iat: moment(),
        verificado: false,
        activo: false
      }
      MongoDB.collection('usuarios').insert(data, function(error, ok) {
        if(error) {
          res.json({msg: 'error', data: error})
        }else{
          twilio.messages.create({
            body: 'Tu codigo de verificacion de Buckstter es: ' + sms + '\nNo compartas este codigo con nadie.',
            from: '+12054330484',
            to: '+51' + numero
          }).then(function(message) {
            //console.log(message)
            console.log("Create => Número: " + numero + " | SMS: " + sms)
            res.json({msg: 'ok'})
          })
        }
      })
    }
  })
})

app.post('/sms', function(req, res) {
  var numero = req.body.numero.trim()
  var sms = req.body.sms.trim()
  MongoDB.collection('usuarios').findOne({'numero': numero, 'sms': sms}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: error})
    }else if(find) {
      var token = jwt.sign({usuario: find._id, activo: false}, config.secret, { expiresIn: '1h' })
      if(find.verificado) {
        res.json({msg: 'ok', data: token})
      }else{
        find.verificado = true
        MongoDB.collection('usuarios').update({'_id': db.Types.ObjectId(find._id)}, find, function(error, updated) {
          if(error) {
            res.json({msg: 'error', data: error})
          }else{
            res.json({msg: 'ok', data: token})
          }
        })
      }
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })
})

api.post('/qr', function(req, res) {
  //Licencia: https://www.sucamec.gob.pe/sel/faces/qr/gamaclic.xhtml?h=37F11B3AEACE3700E2169DC1876EDF21
  //Tarjeta de propiedad: https://www.sucamec.gob.pe/sel/faces/qr/gamactar.xhtml?h=89C974BB9A19C30974756803CD51DD34
  var usuario = req.token.usuario
  var url = req.body.url
  if(url.startsWith('https://www.sucamec.gob.pe/sel/faces/qr')) {
    var titulo, numero, modalidad, restriccion, estado, tipo, marca, modelo, calibre, serie, documento, nombre, emision, vencimiento, modalidades

    request(url, function(error, response, html){
      if(!error) {
        var $ = cheerio.load(html)
        var json
        var tipo = 0
        $('label[style="font-weight: bold;"]').filter(function(){
          var data = $(this)
          titulo = data.text()
          if(titulo == 'LICENCIA DE USO DE ARMAS') {
            tipo = 1
            json = { titulo : "", numero : "", emision : "", vencimiento : "", modalidades : "", estado : "", documento : "", nombre : "" }
          }else{
            tipo = 2
            json = { titulo : "", numero : "", modalidad : "", restriccion : "", estado : "", tipo : "", marca : "", modelo : "", calibre : "", serie : "", situacion: "", documento : "", nombre : "" }
          }
          json.titulo = titulo
        })

        if(tipo == 1) {

          $('label[title="NRO_LIC"]').filter(function(){
            var data = $(this)
            numero = data.text()
            json.numero = numero
          })

          $('label[title="FEC_EMISION"]').filter(function(){
            var data = $(this)
            emision = data.text()
            json.emision = emision
          })

          $('label[title="FEC_VENCIMIENTO"]').filter(function(){
            var data = $(this)
            vencimiento = data.text()
            json.vencimiento = vencimiento
          })

          $('label[title="MODALIDADES"]').filter(function(){
            var data = $(this)
            modalidades = data.text()
            json.modalidades = modalidades
          })

          $('label[title="ESTADO"]').filter(function(){
            var data = $(this)
            estado = data.text()
            json.estado = estado
          })

          $('label[title="DOC_PROPIETARIO"]').filter(function(){
            var data = $(this)
            documento = data.text()
            json.documento = documento
          })

          $('label[title="PORTADOR"]').filter(function(){
            var data = $(this)
            nombre = data.text()
            json.nombre = nombre
          })
        }/*else if(tipo == 2){

        $('label[title="NRO_RUA"]').filter(function(){
            var data = $(this)
            numero = data.text()
            json.numero = numero
          })

          $('label[title="MODALIDAD"]').filter(function(){
            var data = $(this)
            modalidad = data.text()
            json.modalidad = modalidad
          })

          $('label[title="RESTRICCION"]').filter(function(){
            var data = $(this)
            restriccion = data.text()
            json.restriccion = restriccion
          })

          $('label[title="ESTADO"]').filter(function(){
            var data = $(this)
            estado = data.text()
            json.estado = estado
          })

          $('label[title="TIPO"]').filter(function(){
            var data = $(this)
            tipo = data.text()
            json.tipo = tipo
          })

          $('label[title="MARCA"]').filter(function(){
            var data = $(this)
            marca = data.text()
            json.marca = marca
          })

          $('label[title="MODELO"]').filter(function(){
            var data = $(this)
            modelo = data.text()
            json.modelo = modelo
          })

          $('label[title="CALIBRE"]').filter(function(){
            var data = $(this)
            calibre = data.text()
            json.calibre = calibre
          })

          $('label[title="SERIE"]').filter(function(){
            var data = $(this)
            serie = data.text()
            json.serie = serie
          })

          $('label[title="SITUACIÓN"]').filter(function(){
            var data = $(this)
            situacion = data.text()
            json.situacion = situacion
          })

          $('label[title="DOC_PROPIETARIO"]').filter(function(){
            var data = $(this)
            documento = data.text()
            json.documento = documento
          })

          $('label[title="NOMBRES"]').filter(function(){
            var data = $(this)
            nombre = data.text()
            json.nombre = nombre
          })

        }*/
        MongoDB.collection('usuarios').findOne({'_id': db.Types.ObjectId(usuario)}, function(error, find){
          if(error) {
            res.json({msg: 'error', data: error})
          }else if(find && find.verificado) {
            if(tipo == 1) {
              if(json.estado == 'VIGENTE') {
                find.nombre = json.nombre
                find.documento = json.documento
                find.licencia = json.numero
                find.emision = json.emision
                find.vencimiento = json.vencimiento
                find.modalidades = json.modalidades.split(',')
                find.url = url
                MongoDB.collection('usuarios').update({'_id': db.Types.ObjectId(usuario)}, find, function(error, updated) {
                  if(error) {
                    res.json({msg: 'error', data: error})
                  }else{
                    res.json({msg: 'ok', data: {
                      nombre: json.nombre,
                      documento: json.documento,
                      numero: json.numero,
                      vencimiento: json.vencimiento,
                      modalidades: json.modalidades,
                      token: jwt.sign({usuario: find._id, activo: false}, config.secret, { expiresIn: '1h' })
                    }})
                  }
                })
              }else{
                res.json({msg: 'error', data: 'invalid'})
              }
            }else{
              res.json({msg: 'error', data: 'wrong'})
            }
          }else{
            res.json({msg: 'error', data: 'not_found'})
          }
        })
      }else{
        res.json({msg: 'error', data: error})
      }
    })
  }else{
    res.json({msg: 'error', data: 'wrong_url'})
  }
})

api.get('/activar', function(req, res) {
  var usuario = req.token.usuario
  MongoDB.collection('usuarios').findOne({'_id': db.Types.ObjectId(usuario)}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: error})
    }else if(find) {
      var token = jwt.sign({usuario: find._id, activo: true}, config.secret, { expiresIn: '1d' })
      if(find.activo) {
        res.json({msg: 'ok', data: token})
      }else{
        find.activo = true
        MongoDB.collection('usuarios').update({'_id': db.Types.ObjectId(usuario)}, find, function(error, updated) {
          if(error) {
            res.json({msg: 'error', data: error})
          }else{
            res.json({msg: 'ok', data: token})
          }
        })
      }
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })
})

api.get('/inicio', function(req, res) {
  var usuario = req.token.usuario
  var json = {
    ofertas: ['http://192.168.0.105:3000/img/clay.jpg', 'http://192.168.0.105:3000/img/cyber.png'],
    ad: 'https://www.innovapublicidad.es/wp-content/uploads/2016/06/Razones-vender-online-bg.jpg',
    productos: [
      {
        id: '1',
        imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
        titulo: 'Pistola Glock 19 Gen4',
        precio: 'S/ 2.900',
        estado: 'Nuevo',
        tipo: 1
      },{
        id: '2',
        imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
        titulo: 'Pistola Glock 19 Gen4',
        precio: 'S/ 2.900',
        estado: 'Nuevo',
        tipo: 2
      },{
        id: '3',
        imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
        titulo: 'Pistola Glock 19 Gen4',
        precio: 'S/ 2.900',
        estado: 'Nuevo',
        tipo: 3
      }
    ]
  }
  res.json({msg: 'ok', data: json})
  /*MongoDB.collection('usuarios').findOne({'_id': db.Types.ObjectId(usuario)}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: error})
    }else if(find) {
      var token = jwt.sign({usuario: find._id, activo: true}, config.secret, { expiresIn: '1h' })
      if(find.activo) {
        res.json({msg: 'ok', data: token})
      }else{
        find.activo = true
        MongoDB.collection('usuarios').update({'_id': db.Types.ObjectId(usuario)}, find, function(error, updated) {
          if(error) {
            res.json({msg: 'error', data: error})
          }else{
            res.json({msg: 'ok', data: token})
          }
        })
      }
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })*/
})

api.get('/buscar', function(req, res) {
  var usuario = req.token.usuario
  var query = req.query.s
  var json = [
    {
      id: '1',
      imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
      titulo: 'Pistola Glock 19 Gen4',
      precio: 'S/ 2.900',
      estado: 'Nuevo',
      tipo: 1
    },{
      id: '2',
      imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
      titulo: 'Pistola Glock 19 Gen4',
      precio: 'S/ 2.900',
      estado: 'Nuevo',
      tipo: 2
    },{
      id: '3',
      imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
      titulo: 'Pistola Glock 19 Gen4',
      precio: 'S/ 2.900',
      estado: 'Nuevo',
      tipo: 3
    }
  ]
  res.json({msg: 'ok', data: json})
})

api.get('/detalle/:id', function(req, res) {
  console.log('request');
  var usuario = req.token.usuario
  var id = req.params.id
  var json = {
    id: '1',
    imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
    titulo: 'Pistola Glock 19 Gen4',
    precio: 'S/ 2.900',
    estado: 'Nuevo',
    tipo: 1
  }
  res.json({msg: 'ok', data: json})
})

api.get('/publicaciones', function(req, res) {
  var usuario = req.token.usuario
  var json = [{
    id: '1',
    imagen: 'https://www.budsgunshop.com/catalog/images/719017027.jpg',
    titulo: 'Pistola Glock 19 Gen4',
    precio: 'S/ 2.900',
    estado: 'Nuevo',
    tipo: 1
  }]
  res.json({msg: 'ok', data: json})
})

//SocketIO
io.on('connection', function(socket) {
  socket.on('get', function() {
    socket.emit('get', {data: []})
  })
})

//Tareas
new CronJob('00 * * * * *', function() {
  //actualizar()
}, null, true, 'America/Lima')

//Server
http.listen(config.port, function() {
  console.log('Servidor montado *:' + config.port)
})
